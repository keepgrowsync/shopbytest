/*
 * © NHN Commerce Corp. All rights reserved.
 * NHN Commerce Corp. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @author Choisohyun
 * @since 2022-07
 */
$(() => {
  const $orderResults = $('#orderResults');
  const $orderResultsTemplate = $('#orderResultsTemplate');

  shopby.previousList = {
    dateRange: {},
    page: {},
    initiate() {
      this.dateRange = new shopby.dateRange(
        '#dateSelector',
        this.onClickSearchBtn.bind(this),
        undefined,
        undefined,
        null,
        this.dateButtons,
      );
      this.initReadMore();

      this.fetchPreviousOrders().then(response => {
        if (response && response.error) {
          shopby.alert('인증 정보가 만료되었습니다.', shopby.goLogin);
        } else {
          this.onClickSearchBtn();
          this._renderOrders(response.data);
        }
      });
    },
    initReadMore() {
      const REQUEST_COUNT = 5;
      this.page = new shopby.readMore(this.fetchPreviousOrders.bind(this), '#readMore', REQUEST_COUNT);
    },

    _renderOrders(orders) {
      const data = orders.contents.map(order => {
        // 같은 주문이면 orderOptions 에 존재하는 0번째 값으로 사용
        const [firstOrder] = order.orderOptions;

        return {
          ...order,
          orderYmdt: firstOrder.orderYmdt.substr(0, 10),
          orderNo: order.orderNo,
          productName: shopby.utils.substrWithPostFix(firstOrder.productName),
          optionTextInfo: shopby.utils.createOptionText(firstOrder),
          orderCnt: firstOrder.orderCnt,
          buyAmt: order.salePrice,
          orderStatusTypeLabel: this._createOrderStatusLabel(firstOrder.orderStatusType),
        };
      });
      const ordersGroupByOrderNo = shopby.utils.groupByKeyArray(data, 'orderNo', 'orders').map(orderGroup => ({
        ...orderGroup,
        orderCntSum: orderGroup.orders.length,
      }));

      const compiled = Handlebars.compile($orderResultsTemplate.html());
      $orderResults.html('');
      $orderResults.append(compiled(ordersGroupByOrderNo));
      $('#totalcount').text(orders.totalCount);

      this._bindEvents();
    },
    _bindEvents() {
      $('#btn_write').on('click', this.onClickInquiryBtn);
    },

    onClickInquiryBtn(event) {
      event.stopImmediatePropagation();
      shopby.popup('inquiry', {}, data => {
        if (data && data.state === 'close') return;
        window.location.href = '/pages/my/inquiries.html';
      });
    },

    _createOrderStatusLabel(orderStatusType) {
      const orderStatusTypeLabels = shopby.cache.getMall().orderStatusType;
      const statusType = orderStatusTypeLabels.find(({ value }) => value === orderStatusType);

      return statusType ? statusType.label : '';
    },

    async onClickSearchBtn() {
      const { start, end } = this.dateRange;
      const { pageNumber } = this.page;
      shopby.utils.pushState({ start, end, pageNumber });

      const { data: orders } = await this.fetchPreviousOrders();
      this.page.render(orders.totalCount);
      this._renderOrders(orders);
    },

    fetchPreviousOrders() {
      return shopby.api.order.getPreviousOrders({
        queryString: {
          page: this.page.pageNumber,
          size: this.page.pageSize,
          startYmd: this.dateRange.start,
          endYmd: this.dateRange.end,
          hasTotalCount: true,
        },
      });
    },

    get dateButtons() {
      return {
        day3: {
          label: '3일',
          value: '-3',
          unit: 'day',
        },
        week: {
          label: '7일',
          value: '-7',
          unit: 'day',
        },
        day15: {
          label: '15일',
          value: '-15',
          unit: 'day',
        },
        month: {
          label: '1개월',
          value: '-1',
          unit: 'month',
        },
        month3: {
          label: '3개월',
          value: '-3',
          unit: 'month',
        },
      };
    },
  };

  shopby.start.initiate(shopby.previousList.initiate.bind(shopby.previousList));
});

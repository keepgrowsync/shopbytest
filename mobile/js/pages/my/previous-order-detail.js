/*
 *  © 2021. NHN Commerce Corp. All rights reserved.
 *  NHN Corp. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *  @author choisohyun
 *  @since 2022.7
 *
 */

$(() => {
  shopby.my.previousOrderDetail = {
    orderNo: shopby.utils.getUrlParam('orderNo'),
    claimImages: null,
    refundInfos: null,
    additionalPayInfos: null,
    isMember: false,
    async initiate() {
      if (!this.orderNo) {
        location.href = '/pages/my/previous-list.html';
      }
      await this.render();
      this.bindEvents();
    },

    async render() {
      $('.spanOrderNo').text(this.orderNo);
      const {
        orderProductInfo,
        ordererInfo,
        shippingAddressInfo,
        paymentInfo,
        claimInfo,
      } = await this.generateOrderDetail();

      $('#orderDetailTable').render(orderProductInfo);
      $('#orderMemberInfo').render(ordererInfo);
      $('#shippingAddressInfo').render(shippingAddressInfo);
      $('#paymentInfo').render(paymentInfo);
      $('#claimInfo').render(claimInfo);
    },
    bindEvents() {
      $('.orderlist_wrap').on('click', event => this.onClickInquiryBtn(event));
    },

    onClickInquiryBtn(event) {
      const isInquiryBtn = $(event.target).closest('.inquiryBtn').length > 0;

      if (!isInquiryBtn) return;

      shopby.popup('inquiry', {}, data => {
        if (data && data.state === 'close') return;
        window.location.href = '/pages/my/inquiries.html';
      });
    },

    async generateOrderDetail() {
      const { data } = await this.getOrderDetail();

      return {
        orderProductInfo: this._orderProductInfo(data),
        ordererInfo: this._ordererInfo(data),
        shippingAddressInfo: this._shippingAddressInfo(data),
        paymentInfo: this._paymentInfo(data),
        claimInfo: this._claimInfo(data),
      };
    },

    async getOrderDetail() {
      try {
        const apiKey = shopby.logined() ? 'getPreviousOrdersOrderNo' : 'getPreviousOrdersGuestOrderNo';

        const request = {
          pathVariable: { orderNo: this.orderNo },
        };

        return await shopby.api.order[apiKey](request);
      } catch (e) {
        location.href = '/pages/login/login.html';
        throw e;
      }
    },

    _orderProductInfo(orders) {
      return orders.orderProduct.map(order => {
        return {
          orderYmdt: orders.paymentMethod.orderYmdt.substr(0, 10),
          orderNo: this.orderNo,
          rowSpan: orders.orderProduct.length,
          productName: shopby.utils.substrWithPostFix(order.productName),
          optionTextInfo: shopby.utils.createOptionText(order),
          orderCnt: order.orderCnt,
          buyAmt: order.salePrice,
          orderStatusTypeLabel: this._createOrderStatusLabel(order.orderStatusType),
          pgType: order.pgType,
        };
      });
    },

    _ordererInfo(order) {
      return {
        ...order.orderer,
      };
    },

    _shippingAddressInfo(order) {
      return [...order.receiver];
    },

    _paymentInfo({ firstPayment, paymentMethod }) {
      const {
        accumulationConfig: { accumulationName, accumulationUnit },
      } = shopby.cache.getMall();

      return {
        firstPayment: { ...firstPayment },
        paymentMethod: { ...paymentMethod },
        accumulationName,
        accumulationUnit,
      };
    },

    _claimInfo(order) {
      if (!order.refund) {
        return {
          refundType: 'ZERO_REFUND',
        };
      }

      return {
        refund: { ...order.refund, refundAccountInfo: this._createRefundAccountText(order.refund) },
      };
    },

    _createOrderStatusLabel(orderStatusType) {
      const orderStatusTypeLabels = shopby.cache.getMall().orderStatusType;

      return orderStatusTypeLabels.find(({ value }) => value === orderStatusType).label;
    },
    _createRefundAccountText({ refundTypeLabel, refundBankAccount }) {
      const noAccountInfo =
        !refundBankAccount ||
        !refundBankAccount.bank ||
        !refundBankAccount.account ||
        !refundBankAccount.bankDepositorName;
      if (noAccountInfo) {
        return refundTypeLabel;
      }
      return `${refundTypeLabel} - ${refundBankAccount.bank} / ${refundBankAccount.account} / ${refundBankAccount.bankDepositorName}`;
    },

    /**
     * Reference: /my/coupons.js
     * @my :  마이페이지 공통 로직
     */
    my: {
      initiate() {
        shopby.my.menu.init('#myPageLeftMenu');
      },
    },
  };

  shopby.start.initiate(shopby.my.previousOrderDetail.initiate.bind(shopby.my.previousOrderDetail));
});
